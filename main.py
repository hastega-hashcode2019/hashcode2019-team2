import copy
import numpy

n_of_slides = 0
slides = []
current_best_slide = None
slideshow = ""
tot_slides_in_slideshow = 1


class Tag:
    def __init__(self, text):
        self.text = text

    def __eq__(self, other):
        return self.text == other.text

    def __repr__(self):
        return str(self.text)


    def __hash__(self):
        return hash(self.text)


class Slide:
    def __init__(self, i, o, n):
        self.index = str(i)
        self.orientation = o
        self.tot_tags = int(n)
        self.tags = []

    def __repr__(self):
        str_tags = ""
        for t in self.tags:
            str_tags += str(t) + " "
        return str(self.orientation) + " " + str(self.tot_tags) + " " + str_tags


def generate_output(output):
    outputfile = open("output/c_memorable_moments.out", "w")
    outputfile.write(str(output))
    outputfile.close()

def getNOfTags(p1, p2):
    tags = p1.tags + p2.tags
    tags = list(set(tags))
    return len(tags)

def couple(p1, p2):
    tags = p1.tags + p2.tags
    tags = list(set(tags))
    photo = Slide(str(p1.index) + " " + str(p2.index), "VV", len(tags))
    photo.tags = tags

    return photo


filename = "input/c_memorable_moments.txt"
textfile = open(filename, "r").read()
textfile = textfile.split('\n')
firstRow = textfile[0]
n_of_slides = int(firstRow)

for line in range(1, len(textfile)-1):
    cells = textfile[line].split(" ")
    orientation = str(cells[0])
    tot_tags = str(cells[1])
    slide = Slide(line - 1, orientation, tot_tags)
    for cell in range(2, len(cells)):
        tag = Tag(str(cells[cell]))
        slide.tags.append(tag)
    slides.append(slide)


horizontal = []
vertical = []

for photo in slides:
    if photo.orientation == 'V':
        vertical.append(photo)
    else:
        horizontal.append(photo)


newVertical = []
nTags = []
if len(vertical) != 0:
    for i in range(0, len(vertical) - 1, 2):
        #slide.append(couple(photos[i], photos[i+1]))
        nTags.append(vertical[i].tot_tags)
        nTags.append(vertical[i+1].tot_tags)
    stDev = numpy.std(numpy.array(nTags).astype(numpy.float))
    mOfPhoto = sum(nTags) / len(nTags)

    for i in range(0, len(vertical)-2):
        print i
        for j in range(i+1,len(vertical)-1):
            if j != i:
                tags = getNOfTags(vertical[i], vertical[j])
                # print tags
                # print (mOfPhoto*2 - stDev)
                # print (mOfPhoto*2 + stDev)
                if tags > (mOfPhoto*2 - stDev) and tags < (mOfPhoto*2 + stDev):
                    newVertical.append(couple(vertical[i], vertical[j]))
                    vertical.remove(vertical[j])
                    break
slides = newVertical + horizontal

slide = slides[0]
slides.remove(slide)
while len(slides) != 0:
    # print str(slide)
    mid_tags = int(slide.tot_tags) / 2
    prev_common_abs = 0
    prev_uncommon_abs = 0
    for s in slides:
        # print str(s)
        common_tags = 0
        uncommon_tags = 0
        for tag in s.tags:
            if tag in slide.tags:
                common_tags += 1
            else:
                uncommon_tags += 1

        if prev_common_abs is not 0:
            if abs(common_tags - mid_tags) < prev_common_abs:
                current_best_slide = s
                prev_common_abs = abs(common_tags - mid_tags)
        else:
            current_best_slide = s
            prev_common_abs = abs(common_tags - mid_tags)

        # print str(common_tags)
        # print str(uncommon_tags)

    if current_best_slide is not None:
        slides.remove(current_best_slide)
        tot_slides_in_slideshow += 1
        slideshow += str(current_best_slide.index) + "\n"
        print "Tot: " + str(tot_slides_in_slideshow)
        slide = current_best_slide
        current_best_slide = None

generate_output(str(tot_slides_in_slideshow) + "\n" + str(slideshow))
